package main

import "fmt"

func main() {
	var listNode1 *ListNode = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 2,
			Next: &ListNode{
				Val: 4,
			},
		},
	}

	var listNode2 *ListNode = &ListNode{
		Val: 1,
		Next: &ListNode{
			Val: 3,
			Next: &ListNode{
				Val: 4,
			},
		},
	}

	// result := mergeTwoLists(listNode1, listNode2)
	result := mergeTwoListsV2(listNode1, listNode2)

	for i := result; i != nil; i = i.Next {
		fmt.Println(i.Val)
	}
}

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeTwoLists(list1 *ListNode, list2 *ListNode) *ListNode {
	var (
		result    *ListNode
		tail      *ListNode
		temp      *ListNode
		list1Temp *ListNode = list1
		list2Temp *ListNode = list2
		condition bool      = true
	)

	for condition {
		if list1Temp != nil && list2Temp != nil {
			if list1Temp.Val < list2Temp.Val {
				temp = &ListNode{Val: list1Temp.Val}

				list1Temp = list1Temp.Next
			} else {
				temp = &ListNode{Val: list2Temp.Val}

				list2Temp = list2Temp.Next
			}

			if result == nil {
				result = temp
				tail = temp
			} else {
				tail.Next = temp
				tail = tail.Next
			}
		} else if list1Temp != nil {
			if result == nil {
				result = list1Temp
			} else {
				tail.Next = list1Temp
			}

			condition = false
		} else if list2Temp != nil {
			if result == nil {
				result = list2Temp
			} else {
				tail.Next = list2Temp
			}

			condition = false
		} else {
			condition = false
		}
	}

	return result
}

func mergeTwoListsV2(list1 *ListNode, list2 *ListNode) *ListNode {
	return mergeTwoListsRec(list1, list2)
}

func mergeTwoListsRec(list1 *ListNode, list2 *ListNode) *ListNode {
	if list1 != nil && list2 != nil && list1.Val <= list2.Val {
		result := &ListNode{Val: list1.Val}
		result.Next = mergeTwoListsRec(list1.Next, list2)

		return result
	} else if list1 != nil && list2 != nil && list2.Val < list1.Val {
		result := &ListNode{Val: list2.Val}
		result.Next = mergeTwoListsRec(list1, list2.Next)

		return result
	} else if list1 != nil {
		return list1
	} else if list2 != nil {
		return list2
	} else {
		return nil
	}
}
